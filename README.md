# Lightning
Lightning is the source code repository for Yext- Migration to Lightning Experience.
The source of this repository is managed by Grazitti.

Part of this repository there is a build chain enabling deployment to different Salesforce environments.
Building (deploying) to a Salesforce environment is a two-step process:

- Identifying differences, creating build files
- Deploying the build to a Salesforce environment

## Getting started with the build chain
### Clone the repository
* Before you could run git commands on cli or any other medium, configure SSH connection between GitHub remote and your local machine
* Get or create your SSH public kep by referring to documentation at [link](https://docs.joyent.com/public-cloud/getting-started/ssh-keys/generating-an-ssh-key-manually)
* After you accuire the public SSH key follow the instruction to add it to your GitHub account at [link](https://help.github.com/en/enterprise/2.18/user/authenticating-to-github/adding-a-new-ssh-key-to-your-github-account)
* Do not forget to enable Yext SSO for the newly added key before moving on to the next step
* Create a folder where you want to clone the repository, open terminal and change the directory to the same
* Run `git clone git@github.com:yext/lightning.git` in the terminal to clone the repository
### Configure "infinit-project.json" file
* Update the "folderPath" property in "gitConfig" to point to the "lightning" folder of the cloned repository
```JSON
{
    "gitConfig": {
        "folderPath": "****",
    }
}
```
* Update the "name", "targetFolder", "username", "password" and "token" in the "targets"
  * name- Given name to the build configuration, used as the target attribute
  * targetFolder- folder pointing to the build files
  * username- username to use for the deployment
  * password- password to use for the deployment
  * token- security token to use for the deployment (leave blank if not required)
```JSON
{
    "targets": [
        {
            "name": "dexter",
            "targetFolder": "*****",
            "connection": {
                "endPoint": "****.salesforce.com",
                "username": "****",
                "password": "****",
                "token": "****"
            }
        }
    ]
}
```
### Generate build files
* Open terminal and change the directory to the "lightning" folder of the cloned repository
* Run `./infinit git diff` for mac terminal and `java -jar "tools\infinit\infinit-obs.jar" git diff` for windows cmd to create build package, this will create the 'deployment' folder and place the differences between the current head and the commit hash provided in the "infinit-project.json"
```console
Finding diff entries between current head and commit hash: 0d3acd9578185dcb5f983089b140489889ca4c49

Total diff entries: 3

+ salesforce/src/classes/AccountAgreementController.cls
+ salesforce/src/classes/AccountAgreementController.cls-meta.xml
+ salesforce/src/classes/AccountAgreementControllerTest.cls
+ salesforce/src/classes/AccountAgreementControllerTest.cls-meta.xml
```
### Perform deployment
* Run `./infinit deployment start target=dexter` for mac terminal and `java -jar "tools\infinit\infinit-obs.jar" deployment start target=dexter` for windows cmd to start the deployment process to the target Salesforce environment. Please note this command will use the targets specified in the "infinit-project.json"
```console
Connecting to target: DEXTER

Connected Successfully

Reading files from: /Users/pgodara/Desktop/project/lightning/deployment/salesforce

Sending file to Force.com

Checking deployment Status:
Validating  Status: InProgress, this is poll 1 Components (total/deployed/errors): 0 / 0 / 0
Validating  Status: Succeeded, this is poll 2 Components (total/deployed/errors): 2 / 2 / 0
Validation Successful
```
* Once you have verified that the pieces are wired correctly, update the "checkOnly" attribute to false (true value will perform a validate only deployment) in the "infinit-project.json" to perform an actual deployment.
```json
{
    "targets": [
        {
            "options": {
                "ignoreWarnings": false,
                "purgeOnDelete": false,
                "rollbackOnError": true,
                "allowMissingFiles": false,
                "autoUpdatePackage": true,
                "checkOnly": false,
                "testLevel": "NoTestRun"
            }
        }
    ]
}
```
* Note- Log file for each deployment can be found in the folder directory- tools/infinit/deployments

### Describing "infinit-project.json"
| JSON Path | Description |
| --- | --- |
| `instance` | Name or given alias for the connected force.com environment |
| `logLevel` | Log level to fine tune debugging, possible value- <br/>SEVERE, WARNING, INFO, CONFIG, FINE, FINER or FINEST |
| `connection` | User credentials for the connected force.com environment |
| `connection.endPoint` | Endpoint for the connected force.com environment <br/>\[test\|login\].salesforce.com or custom domain if applicable |
| `connection.username` | Username for the connected force.com environment |
| `connection.password` | Password for the connected force.com environment |
| `connection.token` | Security token for the connected force.com environment (if required, else leave blank i.e. "") |
| `gitConfig` | Configuration used for `git diff` command |
| `gitConfig.folderPath` | Root level folder which is a GIT directory |
| `gitConfig.sourceFolderPath` | Relative path for the source folder containing salesforce metadata source files |
| `gitConfig.commitId` | Commit hash to perfrom a diff after and create build folder with the changes |
| `gitConfig.newCommitId` | Commit hash to perfrom a diff until and create build folder with the changes (leave blank or undefined to use latest commit hash) |
| `gitConfig.version` | API version to be used in package.xml |
| `gitConfig.filterFolderPaths` | Array of string to limit build files to be pulled from specified folders |
| `gitConfig.excludeFolderPaths` | Array of string to filter out build files to be pulled from specified folders |
| `gitConfig.pullMetaXMLForTypes` | Array of string for folders to pull meta xml files along with the source files |
| `gitConfig.pullFolderForTypes` | Array of string for folders to pull complete folder for any corresponding file change |
| `targets` | Array of deployment targets used in `deployment start` command |
| `targets.name` | Name or given alias to the deployment target a force.com environment, used as a value to the "target" parameter in `deployment start` command |
| `targets.targetFolder` | Source folder for the deployment, usually points to the build folder but not restricted and can point to any folder directory in the system |
| `targets.connection` | User credentials for the specified deployment target |
| `targets.connection.endPoint` | Endpoint for the specified deployment target <br/>\[test\|login\].salesforce.com or custom domain if applicable |
| `targets.connection.username` | Username for the specified deployment target |
| `targets.connection.password` | Password for the specified deployment target |
| `targets.connection.token` | Security token for the specified deployment target (if required, else leave blank i.e. "") |
| `targets.options` | Deployment option to be used in the for the specified deployment target |
| `targets.options.ignoreWarnings` | Indicates whether a warning should allow a deployment/validation to complete successfully (true) or not (false) |
| `targets.options.purgeOnDelete` | If true, the deleted components in the destructiveChanges.xml manifest file aren't stored in the Recycle Bin. Instead, they become immediately eligible for deletion |
| `targets.options.rollbackOnError` | Indicates whether any failure causes a complete rollback (true) or not (false) |
| `targets.options.allowMissingFiles` | If files that are specified in package.xml are not in the build folder, specifies whether a deployment can still succeed |
| `targets.options.autoUpdatePackage` | If a file is in the build folder but not specified in package.xml, specifies whether the file is automatically added to the package |
| `targets.options.checkOnly` | Set to true to perform a test deployment (validation) of components without saving the components in the deployment target |
| `targets.options.testLevel` | Specifies which tests are run as part of a deployment. The test level is enforced regardless of the types of components that are present in the deployment package. Valid values are- <br/>NoTestRun, RunLocalTests, RunAllTestsInOrg or RunSpecifiedTests |
| `targets.options.runTests` | Array of string for a list of Apex tests to run during deployment. Specify the class name, one name per instance. To use this option, set `testLevel` to "RunSpecifiedTests" else leave it undefined or empty |
